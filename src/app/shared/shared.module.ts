import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { FuseHighlightModule } from '@fuse/components/highlight';
import { FuseCardModule } from '@fuse/components/card';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatNativeDateModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatBadgeModule} from '@angular/material/badge';


import { NgxSpinnerModule } from 'ngx-spinner';

import { ThaiDatePipe } from './pipes/thai-date-pipe';
import { ItemFormatPipe } from './pipes/item-format-pipe';

@NgModule({
    declarations: [ThaiDatePipe, ItemFormatPipe],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        FuseHighlightModule,
        FuseCardModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatRadioModule,
        MatCheckboxModule,
        MatTabsModule,
        MatMomentDateModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatDialogModule,MatBadgeModule,
        MatGridListModule,
        MatPaginatorModule,
        NgxSpinnerModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        // MatLuxonDateModule,
        MatMenuModule,
        MatSelectModule,
        FuseHighlightModule,
        FuseCardModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatRadioModule,
        MatCheckboxModule,
        MatTabsModule,
        MatMomentDateModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatDialogModule,MatBadgeModule,
        MatGridListModule,
        ThaiDatePipe,
        ItemFormatPipe,
        MatPaginatorModule,
        NgxSpinnerModule
    ]
})
export class SharedModule {
}
