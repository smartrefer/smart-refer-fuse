import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetPersonImcService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async select() {
    const _url = `${this.apiUrl}/person_imc/select`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_hcode(hcode:any) {
    const _url = `${this.apiUrl}/person_imc/select_hcode?hcode=${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_hcode_status(hcode:any,status:any,s_date:any,e_date:any) {
    let _url:any
    if(status == undefined || !status){
      _url = `${this.apiUrl}/person_imc/select_hcode_status?hcode=${hcode}&status=&s_date=${s_date}&e_date=${e_date}`;   
    }else{
      _url = `${this.apiUrl}/person_imc/select_hcode_status?hcode=${hcode}&status=${status}&s_date=${s_date}&e_date=${e_date}`;   
    }
    // console.log(_url);
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_status(cid:any,status:any,s_date:any,e_date:any) {
    const _url = `${this.apiUrl}/person_imc/select_status?cid=${cid}&status=${status}&s_date=${s_date}&e_date=${e_date}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_cid(cid:any) {
    const _url = `${this.apiUrl}/person_imc/select_cid?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async person_cid(cid:any) {
    const _url = `${this.apiUrl}/person_imc/person_cid?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_register(datas:any,cid:any) {
    const _url = `${this.apiUrl}/person_imc/select_register?cid=${cid}`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  }

  async onSave(datas:any) {
    const _url = `${this.apiUrl}/person_imc/insert`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  }

  async onUpdate(datas:any,imc_id:any) {
    const _url = `${this.apiUrl}/person_imc/update/${imc_id}`;
    return this.httpClient.put(_url,datas,this.httpOptions).toPromise();
  }

  async onDelete(imc_id:any) {
    const _url = `${this.apiUrl}/person_imc/delete/${imc_id}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  
}
