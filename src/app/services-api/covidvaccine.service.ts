import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CovidvaccineService {
  httpOptions: any;

  constructor(private httpClient: HttpClient) {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJ1Ym9ucHJvbXB0IiwiaWF0IjoxNjYyMTIxMTY5LCJleHAiOjE2OTM2Nzg3Njl9.6ed1H86KOHCmJ0qu-3uKUceESq7RQ0JZBAKzPTmtTVs' 
      })
    };

  }

  async covidvaccine(cid:any) {
    const _url = `http://203.113.117.66/api/ubonprompt/ImmunizationTarget/person?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }
}
