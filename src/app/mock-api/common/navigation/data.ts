/* eslint-disable */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboards',
        title: 'Dashboards',
        subtitle: 'บอร์ดรายงานพื้นฐาน',
        type: 'group',
        icon: 'heroicons_outline:chart-bar',
        children: [
            {
                id: 'dashboards.รายงาน',
                title: 'สรุปการส่งต่อ',
                type: 'basic',
                icon: 'heroicons_outline:clipboard-check',
                link: '/dashboards/project'
            },
         
        ]
    },
    {
        id: 'divider-1',
        type: 'divider'
    },
    {
        id: 'apps',
        title: 'Refer Type',
        subtitle: 'ประเภทการรับส่งต่อ',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'apps.academy',
                title: 'ส่งต่อ',
                type: 'basic',
                icon: 'mat_outline:airline_seat_flat_angled',
                link: '/refer/referout',
                badge: {
                    title: '20',
                    classes: 'px-2 bg-pink-600 text-white rounded-full'
                }
            },
            {
                id: 'apps.chat',
                title: 'รับส่งต่อ',
                type: 'basic',
                icon: 'mat_outline:rv_hookup',
                link: '/refer/referin',
                badge: {
                    title: '25',
                    classes: 'px-2 bg-pink-600 text-white rounded-full'
                }
            },
            {
                id: 'apps.contacts',
                title: 'ส่งกลับ',
                type: 'basic',
                icon: 'mat_outline:airline_seat_flat',
                link: '/refer/referback',
                badge: {
                    title: '2',
                    classes: 'px-2 bg-pink-600 text-white rounded-full'
                }
            },

            {
                id: 'apps.file-manager',
                title: 'รับส่งกลับ',
                type: 'basic',
                icon: 'mat_outline:airline_seat_recline_extra',
                link: '/refer/referreceive',
                badge: {
                    title: '7',
                    classes: 'px-2 bg-pink-600 text-white rounded-full'
                }
            },
          
        ]
    },

    {
        id: 'divider-1',
        type: 'divider'
    },
    {
        id: 'documentation',
        title: 'Documents',
        subtitle: 'คู่มือการติดตั้งใช้งาน',
        type: 'group',
        icon: 'heroicons_outline:support',
        children: [
            {
                id: 'documentation.help-center',
                title: 'Help Center',
                type: 'collapsable',
                icon: 'heroicons_outline:support',
                link: '/apps/help-center',
                children: [
                    {
                        id: 'apps.help-center.home',
                        title: 'Home',
                        type: 'basic',
                        link: '/apps/help-center',
                        exactMatch: true
                    },
                    {
                        id: 'apps.help-center.faqs',
                        title: 'FAQs',
                        type: 'basic',
                        link: '/apps/help-center/faqs'
                    },
                    {
                        id: 'apps.help-center.guides',
                        title: 'Guides',
                        type: 'basic',
                        link: '/apps/help-center/guides'
                    },
                    {
                        id: 'apps.help-center.eval',
                        title: 'Eval',
                        type: 'basic',
                        link: '/evaluate/evaluate'
                    },
                    {
                        id: 'apps.help-center.support',
                        title: 'Support',
                        type: 'basic',
                        link: '/apps/help-center/support'
                    }
                ]
            },
            {
                id: 'documentation.changelog',
                title: 'Changelog',
                type: 'basic',
                icon: 'heroicons_outline:speakerphone',
                link: '/docs/changelog',
                badge: {
                    title: '17.1.0',
                    classes: 'px-2 bg-yellow-300 text-black rounded-full'
                }
            },
            {
                id: 'documentation.guides',
                title: 'Guides',
                type: 'basic',
                icon: 'heroicons_outline:book-open',
                link: '/docs/guides'
            },
            {
                id: 'apps.help-center.eval',
                title: 'Eval',
                type: 'basic',
                icon: 'heroicons_outline:book-open',
                link: '/evaluate/'
            },
            {
                id: 'apps.help-center.eval',
                title: 'Eval2',
                type: 'basic',
                icon: 'heroicons_outline:book-open',
                link: '/evaluate/test2'
            },
            {
                id: 'apps.help-center.eval',
                title: 'assess-view',
                type: 'basic',
                icon: 'heroicons_outline:book-open',
                link: '/evaluate/assess-view'
            },

        ]
    },
    {
        id: 'divider-2',
        type: 'divider'
    },
    {
        id: 'navigation-features',
        title: 'Refer Board',
        subtitle: 'เวปรายงาน Smart Refer',
        type: 'group',
        icon: 'heroicons_outline:menu',
        children: [
            {
                id: 'navigation-features.active',
                title: 'Smart Refer Website',
                subtitle: 'รายงานต่างในระบบ',
                icon: 'heroicons_outline:check-circle',
                type: 'basic',
                active: true
            },
            {
                id: 'navigation-features.disabled-collapsable',
                title: 'Disabled collapsable',
                subtitle: 'Some subtitle',
                icon: 'heroicons_outline:check-circle',
                type: 'collapsable',
                disabled: true,
                children: [
                    {
                        id: 'navigation-features.disabled-collapsable.child',
                        title: 'You shouldn\'t be able to see this child',
                        type: 'basic'
                    }
                ]
            },

        ]
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboards',
        title: 'Dashboards',
        tooltip: 'Dashboards',
        type: 'aside',
        icon: 'heroicons_outline:chart-bar',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id: 'apps',
        title: 'ส่งต่อ',
        tooltip: 'ระบบงานต่างๆ',
        type: 'aside',
        icon: 'mat_outline:airline_seat_flat_angled',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id: 'documentation',
        title: 'Docs',
        tooltip: 'เอกสารคู่มือ',
        type: 'aside',
        icon: 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id: 'navigation-features',
        title: 'ReferBoard',
        tooltip: 'ReferBoard',
        type: 'aside',
        icon: 'heroicons_outline:chart-pie',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboards',
        title: 'DASHBOARDS',
        type: 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id: 'apps',
        title: 'ระบบส่งต่อ',
        type: 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id: 'documentation',
        title: 'Docs',
        type: 'aside',
        icon: 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },

    {
        id: 'navigation-features',
        title: 'ReferBoard',
        type: 'aside',
        icon: 'heroicons_outline:chart-bar',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
   

    {
        id: 'referout',
        title: 'ส่งต่อ  ',
        type: 'basic',
        icon: 'mat_outline:airline_seat_flat_angled',
        link: '/refer/referout',
        badge: {
            title: '20',
            classes: 'px-2 bg-pink-600 text-white rounded-full'
        }
    },
    {
        id: 'referin',
        title: 'รับส่งต่อ',
        type: 'basic',
        icon: 'mat_outline:rv_hookup',
        link: '/refer/referin',
        badge: {
            title: '25',
            classes: 'px-2 bg-pink-600 text-white rounded-full'
        }
    },
    {
        id: 'referback',
        title: 'ส่งกลับ',
        type: 'basic',
        icon: 'mat_outline:airline_seat_flat',
        link: '/refer/referback',
        badge: {
            title: '2',
            classes: 'px-2 bg-pink-600 text-white rounded-full'
        }
    },
    {
        id: 'referreceive',
        title: 'รับส่งกลับ',
        type: 'basic',
        icon: 'mat_outline:airline_seat_recline_extra',
        link: '/refer/referreceive',
        badge: {
            title: '7',
            classes: 'px-2 bg-pink-600 text-white rounded-full'
        }
    },
    {
        id: 'others',
        title: 'โปรแกรมต่างๆ',
        type: 'collapsable',
        icon: 'heroicons_outline:color-swatch',
        children: [
            {
                id: 'dashboards',
                title: 'Dashboards',
                type: 'aside',
                icon: 'heroicons_outline:chart-pie',
                children: [
                    {
                        id: 'dashboards.รายงาน',
                        title: 'สรุปการส่งต่อ',
                        type: 'basic',
                        icon: 'heroicons_outline:clipboard-check',
                        link: '/dashboards/project'
                    },
                ]
            },
            {
                id: 'others.appoint',
                title: 'Appoint',
                type: 'basic',
                icon: 'heroicons_outline:chip',
            },
            {
                id: 'others.telemed',
                title: 'TeleMed',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'others.evaluate',
                title: 'Evaluate',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'others.homecare',
                title: 'ส่งเยี่ยมบ้าน',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'others.homefollow',
                title: 'ติดตามเยี่ยมบ้าน',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },

        ]
    },
    {
        id: 'reports',
        title: 'รายงาน',
        type: 'collapsable',
        icon: 'heroicons_solid:chart-pie',
        children: [

            {
                id: 'reports.phr',
                title: 'PHR',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'reports.imc',
                title: 'IMC',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'reports.report',
                title: 'Report',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },
            {
                id: 'reports.referboard',
                title: 'Refer Board',
                type: 'basic',
                icon: 'heroicons_outline:check-circle',
            },


        ]
    },


    {
        id: 'documentation',
        title: 'เอกสาร-คู่มือ',
        type: 'group',
        icon: 'heroicons_outline:book-open',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
  

];
