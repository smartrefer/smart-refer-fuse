import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module'

import { EvaluateRoutingModule } from './evaluate-routing.module';
import { EvaluateComponent } from './evaluate.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { AssessViewComponent } from './assess-view/assess-view.component';


@NgModule({
  declarations: [
    EvaluateComponent,
    TestComponent,
    Test2Component,
    AssessViewComponent
  ],
  imports: [
    CommonModule,
    EvaluateRoutingModule,SharedModule
  ]
})
export class EvaluateModule { }
