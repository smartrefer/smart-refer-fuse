import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EvaluateComponent} from './evaluate.component';
import {TestComponent} from './test/test.component';
import {Test2Component} from './test2/test2.component';
import { AssessViewComponent } from './assess-view/assess-view.component';


const routes: Routes = [
  {
    path: '',
    component: EvaluateComponent,
    children: [
      { path: '', component: TestComponent },
      { path: 'test', component: TestComponent },
      { path: 'test2', component: Test2Component },
      { path: 'assess-view', component: AssessViewComponent },
     
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluateRoutingModule { }
