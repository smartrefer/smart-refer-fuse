import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferbackComponent } from './referback.component';

describe('ReferbackComponent', () => {
  let component: ReferbackComponent;
  let fixture: ComponentFixture<ReferbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferbackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
