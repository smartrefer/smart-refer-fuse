import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferbackRoutingModule } from './referback-routing.module';
import { ReferbackComponent } from './referback.component';


@NgModule({
  declarations: [
    ReferbackComponent
  ],
  imports: [
    CommonModule,
    ReferbackRoutingModule
  ]
})
export class ReferbackModule { }
