import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReferbackComponent} from './referback.component';

const routes: Routes = [
  { path: '', component: ReferbackComponent }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferbackRoutingModule { }
