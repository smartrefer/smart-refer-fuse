import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import { ReferoutRoutingModule } from './referout-routing.module';
import { ReferoutComponent } from './referout.component';
import { ReferOutMainComponent } from './referout-main/referout-main.component';
import { ReferoutCreateComponent } from './referout-create/referout-create.component';
import { ReferoutViewsComponent } from './referout-views/referout-views.component';
import { ReferoutViewsOnlyComponent } from './referout-views-only/referout-views-only.component';


import { AgGridModule } from 'ag-grid-angular';
import { TestComponent } from './test/test.component';
import { CustomPaginationComponent } from '../../../../shared-component/agGrid/custom-pagination/custom-pagination.component';
import {CustomLoadingCellRenderer  } from '../../../../shared-component/agGrid/custom-loading-cell-renderer.component';
import { CustomTooltip } from '../../../../shared-component/agGrid/custom-tooltip.component';



@NgModule({
  declarations: [
    ReferoutComponent,
    ReferOutMainComponent,
    ReferoutCreateComponent,
    ReferoutViewsComponent,
    ReferoutViewsOnlyComponent,
    TestComponent, CustomPaginationComponent,CustomLoadingCellRenderer,CustomTooltip
  ],
  imports: [
    CommonModule,
    ReferoutRoutingModule, SharedModule,  AgGridModule,NgxDaterangepickerMd.forRoot()

  ],
  providers: [
    // CustomPaginationComponent,CustomLoadingCellRenderer
    { provide: CustomPaginationComponent },
    { provide: CustomLoadingCellRenderer },
    { provide: CustomTooltip },

  ],
})
export class ReferoutModule { }
