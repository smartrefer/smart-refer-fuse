import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';

import {
  CellClickedEvent, ColDef, ColGroupDef, GridReadyEvent,
  GridOptions, GetContextMenuItemsParams, MenuItemDef, AnyGridOptions, ILoadingCellRendererParams
} from 'ag-grid-community';
import moment from 'moment';
import { DaterangepickerDirective } from "ngx-daterangepicker-material";
import { Observable } from 'rxjs';
import { ServicesService } from '../../../../../services-api/services.service';
import { KetReferoutService } from '../../../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../../../services-api/ket-attachment.service';
import { AggridLocaleService } from '../../../../../shared/ag-grid-locale.service';
import { CustomPaginationComponent } from '../../../../../shared-component/agGrid/custom-pagination/custom-pagination.component';
import { CustomLoadingCellRenderer } from '../../../../../shared-component/agGrid/custom-loading-cell-renderer.component';
import { CustomTooltip } from '../../../../../shared-component/agGrid/custom-tooltip.component';
import { AlertService } from '../../../../../services/alert.service';
import { Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-refer-main',
  templateUrl: './referout-main.component.html',
  styleUrls: ['./referout-main.component.scss']
})
export class ReferOutMainComponent {

  // DefaultColDef sets props common to all Columns
  defaultColDef: ColDef = {};
  gridOptions: GridOptions;

  // Data that gets displayed in the grid
  rowsData: any[] = [];
  start_out_date: any;
  end_out_date: any;
  sdate: Date;
  edate: Date;
  selectedDateStart: any = { startDate: null, endDate: null }; selectedDateEnd: any = { startDate: null, endDate: null };
  startDate: any;
  endtDate: any;
  hcode: any;
  limitReferOut: any = 2000;

  rowDataEmp: any[] = [];
  // ag-grid
  localeText: any = AggridLocaleService.AG_GRID_LOCALE_EN;

  // For accessing the Grid's API
  @ViewChild(AgGridAngular) agGrid!: AgGridAngular;

  // AG grid
  columnDefsEmp: any;
  // defaultColDef: any;  
  rowSelection: any; gridApi: any; gridColumnApi: any; gridApi3: any; gridColumnApi3: any; gridApi4: any; gridColumnApi4: any;
  pinnedTopRowData: any; pinnedBottomRowData: any; frameworkComponents: any; rowData: any; sideBar: any; rowGroupPanelShow: any;
  pivotPanelShow: any; previousVal: any; currentVal: any;
  overlayLoadingTemplate: any; overlayNoRowsTemplate: any;
  loadingCellRenderer: any = CustomLoadingCellRenderer;
  loadingCellRendererParams: any = {
    loadingMessage: 'One moment please...x',
  };
  public tooltipShowDelay = 0;
  public tooltipHideDelay = 20000;

  columnDefs: (ColDef | ColGroupDef)[] = [];
  /* custom pagination */
  currentPage: any = 0;
  totalPage: any;
  totalRows: any;
  rowCount: number;
  rowsPerPage: number;
  pageStart: number; pageStop: number;
  paginationNumberFormatter: any;
  // pagination
  Offset: number = 0; limit: number = 10000;
  startRow: number; endRow: number;
  paginationPageSize: any = 10;
  pageSizeDatas: any = [10, 20, 30, 50, 100, 200];


  totalRec: any = 1000; infiniteInitialRowCount: any; maxConcurrentDatasourceRequests: any;
  getRowNodeId: any; rowModelType: any; cacheOverflowSize: any;
  maxBlocksInCache: any; cacheBlockSize: any;
  @ViewChild(CustomPaginationComponent) child: CustomPaginationComponent;

  limitDatas: any = [5000, 10000, 20000, 30000, 40000, 50000];

  constructor(
    private http: HttpClient,
    private ketReferoutService: KetReferoutService,
    private servicesService: ServicesService,
    private ketAttachmentService: KetAttachmentService,
    private alertService: AlertService,
    private router: Router,



  ) {
    const startDate = moment().format("YYYY-MM-DD");
    const endDate = moment().format("YYYY-MM-DD");
    this.selectedDateStart.startDate = moment();
    this.selectedDateEnd.endDate = moment();
    // this.startDate = startDate;
    this.startDate = '2023-01-01';
    this.endtDate = endDate;
    this.pageStart = this.Offset + 1;
    this.pageStop = this.paginationPageSize;

    this.paginationNumberFormatter = function (params) {
      return '[' + params.value.toLocaleString() + ']';
    };
    this.getRowNodeId = function (item) {
      return item.id;
    };
    this.gridOptions = {
      onRowDoubleClicked: this.onRowDoubleClicked.bind(this),
      // loadingCellRenderer: this.loadingCellRenderer,
      // loadingCellRendererParams: this.loadingCellRendererParams(this)
      // Add other grid options as needed
    };
  }
  onRowDoubleClicked(params: any) {
    //console.log('Row double-clicked:', params.data); // Access the row data here
    let jsonString = JSON.stringify(params.data);
    this.router.navigate(['/refer/referout/referout-views-only'], { queryParams: { data: jsonString } }    );

  }
  async ngOnInit() {
    await this.getData();
    await this.gridCreate();
  }
  onPageStartold(i) {
    console.log('onPageStart');
    let x = i - this.paginationPageSize
    if (x > 0) {
      this.Offset = x - 1;
      this.limit = this.paginationPageSize;

      this.pageStart = this.Offset + 1;
      this.pageStop = x + this.paginationPageSize - 1;
      this.getData();
    } else {
      this.Offset = 0;
      this.limit = this.paginationPageSize;

      this.pageStart = 1;
      this.pageStop = this.paginationPageSize;
      this.getData();
    }

  }
  onPageStopOld(i) {
    console.log('onPageStop');
    // let x = this.Offset
    this.Offset = i;
    this.limit = this.paginationPageSize;
    this.pageStart = this.Offset + 1;
    this.pageStop = this.paginationPageSize + i;
    this.getData();

  }
  dateChangeStart(e: any) {
    console.log("date change start");
    // console.log(e);
    if (e.startDate != null) {
      let syear = e.startDate.$d.getFullYear();
      let smonth = String(e.startDate.$d.getMonth() + 1).padStart(2, "0");
      let sday = String(e.startDate.$d.getDate()).padStart(2, "0");
      // this.startDate = [syear, smonth, sday].join("-");
      this.startDate = '2023-01-01';
      console.log(this.startDate);
    }
  }
  dateChangeEnd(e: any) {
    console.log("date change end");
    console.log(e);
    if (e.startDate != null) {
      let eyear = e.endDate.$d.getFullYear();
      let emonth = String(e.endDate.$M + 1).padStart(2, "0");
      let eday = String(e.endDate.$D).padStart(2, "0");
      this.endtDate = [eyear, emonth, eday].join("-");
      console.log(this.endtDate);
    }
  }
  async getData() {
    this.rowsData = [];
    let startDate: any = this.startDate;
    let endDate: any = this.endtDate;
    this.hcode = '10962'
    let rs: any;
    try {
      rs = await this.ketReferoutService.select(this.hcode, startDate, endDate, this.limitReferOut);
      console.log(rs);
      this.rowsData = rs;

    } catch (error) {
      console.log(error);

    }
  }
  // Create Ag grid Table
  async gridCreate() {
    this.defaultColDef = {
      minWidth: 1,
      // editable: true,
      resizable: true,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      // floatingFilter: true,
      tooltipComponent: CustomTooltip,
      wrapHeaderText: true,
      autoHeaderHeight: true,
    };
    this.columnDefs = [
      {
        headerName: 'การนำส่ง',
        headerClass: ['font-black', 'text-blue-800', 'text-base', 'justify-center'],

        children: [
          {
            headerName: 'ประเภทผู้ป่วย',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'text-center', 'text-clip'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ประเภทผู้ป่วย ', field1: 'strength_name', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 25,
            width: 25,
            minWidth: 25,
            resizable: false,
            cellRenderer: (params: any) => {
              // console.log(params);
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              // div.id = 'dkid';
              // div.style.padding = '0px';
              // div.style.width = 'auto';
              // div.style.height = 'auto';
              const btnAttach = document.createElement('button');
              let htmlBtn: any;
              let expression: any = params.data.strength_id;
              switch (expression) {
                case '1': {
                  htmlBtn = '<div class="flex items-center justify-center p-0 shadow resuscitate-pt">R</div>';
                  break;
                }
                case '2': {
                  htmlBtn = '<div class="flex items-center justify-center p-0 shadow emergency-pt"> E </div>';
                  break;
                }
                case '3': {
                  htmlBtn = '<div class="flex items-center justify-center p-0 shadow urgency-pt"> U </div>';
                  break;
                }
                case '4': {
                  htmlBtn = '<div class="flex items-center justify-center p-0 shadow semi-urgency-pt"> S </div>';
                  break;
                }
                case '5': {
                  htmlBtn = '<div class="flex items-center justify-center p-0 shadow none-urgency-pt"> N </div>';
                  break;
                }
                default: {
                  htmlBtn = '<div class="flex items-center justify-center p-0  shadow bg-red-700">  </div>';
                  break;
                }
              }
              btnAttach.innerHTML = htmlBtn;
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
              });

              div.appendChild(btnAttach);
              return div;
            }
          },
          {
            headerName: '',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'text-center'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ประเภท Trigage ', field1: 'refer_triage_name', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 40,
            width: 35,
            minWidth: 25,
            cellRenderer: (params: any) => {
              // console.log(params);
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnDoc = document.createElement('button');
              let htmlBtn2: any;
              let expression2: any = params.data.refer_triage_id;
              switch (expression2) {
                case '1': {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow resuscitate-pt"> U </div>';
                  break;
                }
                case '2': {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow emergency-pt"> H </div>';
                  break;
                }
                case '3': {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow urgency-pt"> M </div>';
                  break;
                }
                case '4': {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow semi-urgency-pt"> L </div>';
                  break;
                }
                case '5': {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow none-urgency-pt"> N </div>';
                  break;
                }
                default: {
                  htmlBtn2 = '<div class="flex items-center justify-center p-0 shadow bg-red-700">  </div>';
                  break;
                }
              }
              btnDoc.innerHTML = htmlBtn2;
              btnDoc.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
              });


              div.appendChild(btnDoc);
              return div;
            }
          },
          {
            headerName: 'แนบ',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'pl-1', 'pr-1', 'text-ellipsis'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'แนบเอกสาร ', field1: '', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 40,
            width: 35,
            minWidth: 25,
            resizable: false,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnAttach = document.createElement('button');
              btnAttach.innerHTML = '<div class="flex items-center justify-center rounded bg-neutral-200 w-6 h-6 cursor-pointer hover:bg-violet-400"><i class="fa-solid fa-paperclip"></i></div>';
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
                this.uploadDocument(params.data);
              });
              div.appendChild(btnAttach);
              return div;
            }
          },
          {
            headerName: '',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'pl-1', 'pr-1'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ใบนัด? ', field1: 'attpath_appoint', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 40,
            width: 35,
            minWidth: 25,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const divDoc = document.createElement('button');
              let htmlBtn: any;
              let expression: any = params.data.attpath_appoint;
              switch (expression) {
                case null: {
                  htmlBtn = '<div class="flex items-center justify-center rounded bg-neutral-200 w-6 h-6 cursor-pointer hover:bg-violet-400"><i class="fa-regular fa-file-pdf"></i></div>';
                  break;
                }
                default: {
                  htmlBtn = '<div class="flex items-center justify-center rounded bg-green-500 w-6 h-6 cursor-pointer hover:bg-violet-400"><i class="fa-regular fa-file-pdf"></i></div>';
                  break;
                }
              }
              // console.log(htmlBtn);
              divDoc.innerHTML = htmlBtn;
              divDoc.addEventListener('click', () => {
                console.log(params.data.attpath_appoint);
                if (params.data.attpath_appoint != null) {
                  this.downloadAppoint(params.data.attpath_appoint);
                }
              });
              div.appendChild(divDoc);
              return div;
            }
          },
          {
            headerName: 'ขอนัด',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'pl-1', 'pr-1'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            cellClass: ['justify-items-center'],
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ขอนัด? ', field1: 'refer_appoint', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 40,
            width: 40,
            minWidth: 25,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnAttach = document.createElement('button');
              let htmlBtn: any;
              let expression: any = params.data.refer_appoint;
              switch (expression) {
                case '1': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer appoint-request-red hover:bg-violet-400"><i class="fa-solid fa-calendar-plus"></i></div>';
                  break;
                }
                case '2': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer appoint-request-green hover:bg-violet-400"><i class="fa-solid fa-calendar-plus"></i></div>';
                  break;
                }
                case '3': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer appoint-request-yellow hover:bg-violet-400"><i class="fa-solid fa-calendar-plus"></i></div>';
                  break;
                }
                case '4': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer appoint-request-violet hover:bg-violet-400"><i class="fa-solid fa-calendar-plus"></i></div>';
                  break;
                }
                default: {
                  htmlBtn = '<div></div>';
                  break;
                }
              }

              btnAttach.innerHTML = htmlBtn;
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
              });
              div.appendChild(btnAttach);

              return div;
            }
          },
          {
            headerName: 'นำส่ง',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'pl-1', 'pr-1'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ประเภทการนำส่ง ', field1: 'loads_name', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 40,
            minWidth: 25,
            width: 40,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnAttach = document.createElement('button');
              let htmlBtn: any;
              let expression: any = params.data.loads_id;
              switch (expression) {
                case '1': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-green hover:bg-violet-400"><i class="fa-solid fa-car"></i></div>';
                  break;
                }
                case '2': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-red hover:bg-violet-400"><i class="fas fa-user-nurse"></i></div>';
                  break;
                }
                case '3': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-yellow hover:bg-violet-400"><i class="fas fa-user-md"></i></div>';
                  break;
                }
                case '4': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-violet hover:bg-violet-400"><i class="fas fa-walking"></i></div>';
                  break;
                }
                case '5': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-grey hover:bg-violet-400"><i class="fas fa-people-carry"></i></div>';
                  break;
                }
                case '6': {
                  htmlBtn = '<div class="flex items-center justify-center cursor-pointer loads-blue hover:bg-violet-400"><i class="fas fa-broadcast-tower"></i></div>';
                  break;
                }
                default: {
                  htmlBtn = '<div></div>';
                  break;
                }
              }
              btnAttach.innerHTML = htmlBtn;
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
              });
              div.appendChild(btnAttach);

              return div;
            }
          },
          {
            headerName: 'ต่ออายุใบส่งตัว',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm', 'pl-1', 'pr-1'],
            cellStyle: { overflow: 'visible', 'z-index': '-1', padding: '0px' },
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'ต่ออายุใบส่งตัว ', field1: '', field2Title: '', field2: '',
              field3Title: '', field3: '', field4Title: '', field4: ''
            },
            maxWidth: 60,
            width: 40,
            minWidth: 25,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnAttach = document.createElement('button');
              btnAttach.innerHTML = '<div class="flex items-center justify-center rounded bg-neutral-200 w-6 h-6 cursor-pointer hover:bg-violet-400"><i class="fa-regular fa-file"></i></div>';
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
              });
              div.appendChild(btnAttach);

              return div;
            }
          },

        ]
      },

      {
        headerName: 'ผู้ป่วย',
        headerClass: ['font-black', 'text-blue-800', 'text-base', 'justify-center'],
        children: [
          {
            headerName: 'ปลายทาง',
            field: 'receive_spclty_name',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 120,
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'HN',
            pinned: 'left',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            field: 'hn',
            tooltipField: 'hn',
            // tooltipComponent: 'tooltipComponent',
            tooltipComponentParams: {
              color: '#0d0d0d', field1Title: 'สถานะการับ', field1: 'receive_refer_result_name', field2Title: 'วันที่รับ', field2: 'receive_date',
              field3Title: 'เวลาที่รับ', field3: 'receive_time', field4Title: 'จุดที่รับ', field4: 'receive_spclty_name'
            },
            width: 120,
            minWidth: 100,
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'ชื่อ',
            pinned: 'left',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            field: 'fullname',
            width: 200,
            filter: 'agTextColumnFilter',
          },
        ]
      },
      {
        headerName: 'ใบส่งตัว',
        headerClass: ['font-black', 'text-blue-800', 'text-base', 'justify-center'],
        children: [
          {
            headerName: 'cid',
            headerClass: ['font-bold', 'text-blue-800', 'text-base'],
            width: 160,
            field: 'cid',
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'เลขที่ส่ง',
            headerClass: ['font-bold', 'text-blue-800', 'text-base'],
            width: 160,
            minWidth: 25,
            field: 'refer_no',
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'วันที่นำส่ง',
            headerClass: ['font-bold', 'text-blue-800', 'text-base'],
            width: 120,
            field: 'refer_date',
            filter: 'agTextColumnFilter',
          },
        ]
      },
      {
        headerName: 'สถานบริการ',
        headerClass: ['font-black', 'text-blue-800', 'text-base', 'justify-center'],
        children: [
          {
            headerName: 'ปลายทาง',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 200,
            minWidth: 25,
            field: 'refer_hospname',
            filter: 'agTextColumnFilter',
          },

        ]
      },
      {
        headerName: 'การลงรับ',
        headerClass: ['font-black', 'text-blue-800', 'text-base', 'justify-center'],
        children: [
          {
            headerName: 'เลขที่รับ',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 160,
            minWidth: 25,
            field: 'receive_no',
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'จุดรับ',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 120,
            minWidth: 25,
            field: 'receive_spclty_name',
            filter: 'agTextColumnFilter',
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              div.className = 'flex icon-centered';
              const btnAttach = document.createElement('button');
              let htmlBtn: any;
              let expression: any = params.data.eva;
              switch (expression) {
                case 'no_eva': {
                  htmlBtn = '<div>'+params.data.receive_spclty_name+'</div>';
                  break;
                }                
                default: {
                  htmlBtn = '<div>'+params.data.receive_spclty_name+'<i class="fas fa-file-signature bg-blue-900 p-1 rounded showdow text-white cursor-pointer hover:bg-violet-400"></i><div>';
                 
                  break;
                }
              }
              btnAttach.innerHTML = htmlBtn;
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
                this.evaluateView(params.data);
              });
              div.appendChild(btnAttach);

              return div;
            }
          },
          {
            headerName: 'วันที่รับ',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 120,
            minWidth: 25,
            field: 'receive_date',
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'ยกเลิก',
            field: '',
            headerClass: ['font-bold', 'text-blue-800', 'text-sm'],
            width: 80,
            cellRenderer: (params: any) => {
              const div = document.createElement('div');
              const btnAttach = document.createElement('button');
              let htmlBtn: any;
              const arr_receive_no: string[] = ['0', '2', '3'];
              if (arr_receive_no.includes(params.data.receive_no)) {
                htmlBtn = '<div class="flex items-center justify-center rounded bg-neutral-200 w-6 h-6 cursor-pointer hover:bg-violet-400"><i class="fa-regular fa-trash-can"></i></div>';

              } else {
                htmlBtn = '<div></div>';
              }

              btnAttach.innerHTML = htmlBtn;
              btnAttach.addEventListener('click', () => {
                // this.btnDel(params);
                console.log('dk');
                this.deleteReferOut(params.data.receive_no);
              });
              div.appendChild(btnAttach);

              return div;
            }
          },
        ]
      },

    ];
    this.overlayLoadingTemplate = this.localeText.overlayLoadingTemplate;
    // '<span class="ag-overlay-loading-center">Please wait while your rows are loading</span>';
    this.overlayNoRowsTemplate = this.localeText.overlayNoRowsTemplate;

  }

  // start events grid
  // Example load data from server

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  // Example of consuming Grid Event
  onCellClicked(e: CellClickedEvent): void {
    console.log('cellClicked', e);
  }

  // Example using Grid's API
  clearSelection(): void {
    this.agGrid.api.deselectAll();
  }

  onSelectionChanged(event) {
    console.log('onSelectionChanged');

    // var selectedRows = event.api.getSelectedNodes();
    // var rowCount = event.api.getSelectedNodes().length;
    // this.employee_ment_id = selectedRows[0].data.employee_id
    // var breadcrumb_emp = selectedRows[0].data.emp_fname + ' ' + selectedRows[0].data.emp_lname;
    // this.router.navigate(['home/employee/', breadcrumb_emp]);



    // this.onEdit(selectedRows[0].data);
    // this.getInfoEmpment(this.employee_ment_id);
    // this.getInfoExemption(this.employee_ment_id);
    //nooo window.alert('selection changed, ' + rowCount + ' rows selected');
  }
  async onCellValueChanged(event: any) {
    console.log('data after changes is: ', event.data);
    // let item = event.data;
    // console.log(item);

  }
  onPaginationChanged(event: any) {
    // console.log(this.gridApi);
    if (this.gridApi) {
      // console.log('onPaginationChanged');
      this.currentPage = this.gridApi.paginationGetCurrentPage() + 1;
      // console.log(this.currentPage);
      this.totalPage = this.gridApi.paginationGetTotalPages();
      // console.log(this.totalPage);

      this.rowCount = this.gridApi.paginationGetRowCount();
      // console.log(this.rowCount);

      this.rowsPerPage = this.gridApi.paginationGetPageSize();
      // console.log(this.rowsPerPage);
    }
  }

  onPageSizeChanged(newPageSize: any) {
    var value: any = newPageSize.getElementById('page-size').value;
    this.gridApi.paginationSetPageSize(Number(value));
  }
  onRowSelect(event: any) {

    const navigationExtras: NavigationExtras = {
      state: { data: event.data }
    };

    this.router.navigate(['/referout/referout-views-only'], navigationExtras);

  }
  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    var result: (string | MenuItemDef)[] = [
      {
        // custom item
        name: 'Alert ' + params.value,
        action: () => {
          window.alert('Alerting about ' + params.value);
        },
        cssClasses: ['redFont', 'bold'],
      },
      {
        // custom item
        name: 'Always Disabled',
        disabled: true,
        tooltip:
          'Very long tooltip, did I mention that I am very long, well I am! Long!  Very Long!',
      },
      {
        name: 'Country',
        subMenu: [
          {
            name: 'Ireland',
            action: () => {
              console.log('Ireland was pressed');
            },
            icon: createFlagImg('ie'),
          },
          {
            name: 'UK',
            action: () => {
              console.log('UK was pressed');
            },
            icon: createFlagImg('gb'),
          },
          {
            name: 'France',
            action: () => {
              console.log('France was pressed');
            },
            icon: createFlagImg('fr'),
          },
        ],
      },
      {
        name: 'Person',
        subMenu: [
          {
            name: 'Niall',
            action: () => {
              console.log('Niall was pressed');
            },
          },
          {
            name: 'Sean',
            action: () => {
              console.log('Sean was pressed');
            },
          },
          {
            name: 'John',
            action: () => {
              console.log('John was pressed');
            },
          },
          {
            name: 'Alberto',
            action: () => {
              console.log('Alberto was pressed');
            },
          },
          {
            name: 'Tony',
            action: () => {
              console.log('Tony was pressed');
            },
          },
          {
            name: 'Andrew',
            action: () => {
              console.log('Andrew was pressed');
            },
          },
          {
            name: 'Kev',
            action: () => {
              console.log('Kev was pressed');
            },
          },
          {
            name: 'Will',
            action: () => {
              console.log('Will was pressed');
            },
          },
          {
            name: 'Armaan',
            action: () => {
              console.log('Armaan was pressed');
            },
          },
        ],
      },
      'separator',
      {
        // custom item
        name: 'Windows',
        shortcut: 'Alt + W',
        action: () => {
          console.log('Windows Item Selected');
        },
        icon:
          '<img src="https://www.ag-grid.com/example-assets/skills/windows.png" />',
      },
      {
        // custom item
        name: 'Mac',
        shortcut: 'Alt + M',
        action: () => {
          console.log('Mac Item Selected');
        },
        icon:
          '<img src="https://www.ag-grid.com/example-assets/skills/mac.png"/>',
      },
      'separator',
      {
        // custom item
        name: 'Checked',
        checked: true,
        action: () => {
          console.log('Checked Selected');
        },
        icon:
          '<img src="https://www.ag-grid.com/example-assets/skills/mac.png"/>',
      },
      'copy',
      'separator',
      'chartRange',
    ];
    return result;
  }

  // end of events grid


  async downloadAppoint(filename: any) {
    // console.log(filename);
    let download: any = await this.ketAttachmentService.download(filename);
  }
  uploadDocument(datas: any) {
    // console.log(datas);
    this.ketAttachmentService.link_upload(datas.refer_no, datas.fullname, this.hcode, datas.refer_hospcode);

  }
  async deleteReferOut(_receive_no: any) {
    // console.log('deleteReferOut : ',i);

    let confirm = await this.alertService.confirm("ต้องการลบรายการนี้ ใช่หรือไม่");
    // console.log(confirm);

    if (confirm) {
      let rs = await this.ketReferoutService.onDelete(_receive_no)
      // console.log(rs);
      await this.getData();
      await this.gridCreate();
    }

  }

  evaluateView(params: any) {
    // console.log(datas);
    let jsonString = JSON.stringify(params.data);
    // this.router.navigate(['/home/assess-view']);
    this.router.navigate(['/evaluate/assess-view'], { queryParams: { data: jsonString } });

  }



}

function createFlagImg(flag: string) {
  return (
    '<img border="0" width="15" height="10" src="https://flags.fmcdn.net/data/flags/mini/' +
    flag +
    '.png"/>'
  );
}


