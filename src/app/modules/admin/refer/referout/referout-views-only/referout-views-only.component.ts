import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-referout-views-only',
  templateUrl: './referout-views-only.component.html',
  styleUrls: ['./referout-views-only.component.scss']
})
export class ReferoutViewsOnlyComponent implements OnInit {

  jsonData: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
   // Get the value of the 'order' query parameter   
   let jsonString = this.activatedRoute.snapshot.queryParamMap.get('data');
   const jsonObject = JSON.parse(jsonString);
   this.jsonData = jsonObject;
   console.log(this.jsonData); 
  }

}
