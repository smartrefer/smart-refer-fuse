import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferoutViewsComponent } from './referout-views.component';

describe('ReferoutViewsComponent', () => {
  let component: ReferoutViewsComponent;
  let fixture: ComponentFixture<ReferoutViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferoutViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferoutViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
