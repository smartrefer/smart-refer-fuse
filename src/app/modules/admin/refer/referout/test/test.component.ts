import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { CellClickedEvent, ColDef, ColGroupDef, GridReadyEvent } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { ServicesService } from '../../../../../services-api/services.service';
import { KetReferoutService } from '../../../../../services-api/ket-referout.service';
import { AggridLocaleService } from '../../../../../shared/ag-grid-locale.service';


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent {
  // DefaultColDef sets props common to all Columns
  public defaultColDef: ColDef = {
    sortable: true,
    filter: true,
  };

  // Data that gets displayed in the grid
  rowsData: any[] = [];
  start_out_date: any;
  end_out_date: any;
  sdate: Date;
  edate: Date;
  hcode: any;
  limitReferOut: any = 2000;


  rowDataEmp: any[] = [];
  // ag-grid
  localeText: any = AggridLocaleService.AG_GRID_LOCALE_EN;

  // pagination
  Offset: number = 0; limit: number = 10000;
  startRow: number; endRow: number;
  paginationPageSize: any = 1000;
  pageSizeDatas: any = [100, 500, 1000, 2000, 5000, 10000];
  limitDatas: any = [5000, 10000, 20000, 30000, 40000, 50000];

  // For accessing the Grid's API
  @ViewChild(AgGridAngular) agGrid!: AgGridAngular;

  // AG grid
  columnDefsEmp: any;
  // defaultColDef: any;  
  rowSelection: any; gridApi: any; gridColumnApi: any; gridApi3: any; gridColumnApi3: any; gridApi4: any; gridColumnApi4: any;
  pinnedTopRowData: any; pinnedBottomRowData: any; frameworkComponents: any; rowData: any; sideBar: any; rowGroupPanelShow: any;
  pivotPanelShow: any; previousVal: any; currentVal: any;
  overlayLoadingTemplate: any; overlayNoRowsTemplate: any;

  columnDefs: (ColDef | ColGroupDef)[] = [   ];

  constructor(
    private http: HttpClient,
    private ketReferoutService: KetReferoutService,
    private servicesService: ServicesService,
  ) { }

  async ngOnInit() {
    await this.getInfo();
    await this.agGridEmp();

  }
  async getInfo() {
    this.rowsData = [];
    let startDate: any = '2023-01-01';;
    let endDate: any = '2023-01-10';;
    this.start_out_date = '2023-01-01';
    this.end_out_date = '2023-01-10';
    this.hcode = '10962'
    let rs: any;
    try {
      rs = await this.ketReferoutService.select(this.hcode, startDate, endDate, this.limitReferOut);
      console.log(rs);
      let item: any[] = rs;
      let items: any = [];
      if (item) {
        // console.log(item);
        item.forEach(async v => {
          items.push(v);
        });

        this.rowDataEmp = items;
      } else {
        console.log('no item');
      }


    } catch (error) {
      console.log(error);


    }
  }
  async agGridEmp() {
    this.defaultColDef = {

      minWidth: 100,
      // editable: true,
      resizable: true,
      enableRowGroup: true,
      enablePivot: true,
      enableValue: true,
      sortable: true,
      // floatingFilter: true,
    };
    this.columnDefs = [

      {
        headerName: 'ผู้ป่วย',
        children: [
          {
            headerName: 'ปลายทาง',
            field: 'receive_spclty_name',
            width: 180,
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'HN',
            field: 'hn',           
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'ชื่อ',
            field: 'fullname',
            width: 200,
            filter: 'agTextColumnFilter',
          },
        ]
      },
      {
        headerName: 'ใบส่งตัว',
        children: [
          {
            headerName: 'cid',
            field: 'cid',           
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'เลขที่ส่ง',
            field: 'receive_no',            
            filter: 'agTextColumnFilter',
          },
          {
            headerName: 'วันที่นำส่ง',
            field: 'receive_date',          
            filter: 'agTextColumnFilter',
          },
        ]
      },

    ];
    this.overlayLoadingTemplate =
      '<span class="ag-overlay-loading-center">Please wait while your rows are loading</span>';
    this.overlayNoRowsTemplate =
      '<span style="padding: 10px; border: 2px solid #444; background: lightgoldenrodyellow;">กำลังประมวลผล กรุณารอสักครู่ \'.....\' !!</span>';

  }


  // Example load data from server

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  // Example of consuming Grid Event
  onCellClicked(e: CellClickedEvent): void {
    console.log('cellClicked', e);
  }

  // Example using Grid's API
  clearSelection(): void {
    this.agGrid.api.deselectAll();
  }
}
