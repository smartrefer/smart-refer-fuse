import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReferoutComponent } from './referout.component';
import { ReferOutMainComponent } from './referout-main/referout-main.component';
import { TestComponent } from './test/test.component';
import {ReferoutViewsOnlyComponent} from './referout-views-only/referout-views-only.component'


const routes: Routes = [
  {
    path: '',
    component: ReferoutComponent,
    children: [
      { path: '', component: ReferOutMainComponent },
      { path: 'referout-main', component: ReferOutMainComponent },
      { path: 'test', component: TestComponent },
      { path: 'referout-views-only', component: ReferoutViewsOnlyComponent },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferoutRoutingModule { }
