import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';

import { ReferinRoutingModule } from './referin-routing.module';
import { ReferinComponent } from './referin.component';


@NgModule({
  declarations: [
    ReferinComponent
  ],
  imports: [
    CommonModule,
    ReferinRoutingModule,SharedModule
  ]
})
export class ReferinModule { }
