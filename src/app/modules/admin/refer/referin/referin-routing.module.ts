import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReferinComponent} from './referin.component';

const routes: Routes = [
  { path: '', component: ReferinComponent }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferinRoutingModule { }
