import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferreceiveRoutingModule } from './referreceive-routing.module';
import { ReferreceiveComponent } from './referreceive.component';


@NgModule({
  declarations: [
    ReferreceiveComponent
  ],
  imports: [
    CommonModule,
    ReferreceiveRoutingModule
  ]
})
export class ReferreceiveModule { }
