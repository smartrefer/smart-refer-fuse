import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReferreceiveComponent} from './referreceive.component';

const routes: Routes = [
  { path: '', component: ReferreceiveComponent }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferreceiveRoutingModule { }
