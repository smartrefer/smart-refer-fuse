import { IAfterGuiAttachedParams, ICellRendererParams, RowNode } from '@ag-grid-community/core';
import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
@Injectable({
    providedIn: 'root'
})

export class AgGridBottomRow implements ICellRendererAngularComp {

    public params: any;
    public style: string;

    constructor() { }
    // agInit(params: any): void {
    //     this.params = params;
    //     this.style = this.params.style;
    //   }

    //   refresh(): boolean {
    //     return false;
    //   }
    refresh(params: ICellRendererParams): boolean {
        throw new Error('Method not implemented.');
        // return false;
    }
    agInit(params: ICellRendererParams): void {
        throw new Error('Method not implemented.');
        // this.params = params;
        // this.style = this.params.style;
    }
    afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
        throw new Error('Method not implemented.');
    }


    calculatePinnedBottomData(target: any, params, columnsWithAggregation) {
        // console.log(params.data);
        // console.log(columnsWithAggregation.slice(1));

        // console.log(target);
        //**list of columns fo aggregation**
        // let columnsWithAggregation = [{ el: 'tax_year', typeAg: 'head' }, { el: 'income_from', typeAg: 'sum' }, { el: 'income_to', typeAg: 'count' }];

        columnsWithAggregation.slice(1).forEach(element => {
            //   console.log( element);
            if (element.typeAg == 'sum') {
                params.forEachNodeAfterFilter((rowNode: RowNode) => {
                    if (rowNode.data[element.el]) {
                        // console.log(rowNode.rowIndex);
                        // console.log(typeof rowNode.data[element.el]);
                        // console.log(rowNode.data[element.el]);
                        let typeEl = typeof target[element.el];
                        if (typeEl == 'string' || rowNode.data[element.el] == '0.00' ) {
                            // console.log('this string');
                            // console.log(rowNode.data[element.el]);
                            let n: number = +rowNode.data[element.el];
                            // console.log(n);
                            target[element.el] = Number(target[element.el]) + n;
                            // target[element.el] += parseInt(rowNode.data[element.el]);
                            // console.log(target[element.el]);

                        }else if(rowNode.data[element.el] == null){
                            // console.log('this null');
                            target[element.el] =  target[element.el];
                        }else {
                            // console.log('this Number');
                            let n: number = +rowNode.data[element.el];
                            // console.log(n);
                            target[element.el] = Number(target[element.el]) + n;
                            // console.log(rowNode.data[element.el]);

                            // target[element.el] += rowNode.data[element.el];
                        }
                        // target[element.el] +=Number(rowNode.data[element.el]); 
                    }
                });
            }else if(element.typeAg == 'other'){
                // console.log('other');
                // target[element.el] = null;
                // console.log(element.el);
                params.forEachNodeAfterFilter((rowNode: RowNode) => {
                    if (rowNode.data[element.el]) {
                        // console.log('me');
                        // console.log(rowNode.data);
                        target[element.el] = null;
                    }else{
                        target[element.el] = null;
                    }
                })
                // console.log(target[element.el]);
            } else {
                var i = 1;
                // params.forEachNodeAfterFilter((rowNode: RowNode) => {
                //     if (rowNode.data[element.el]) {
                //         i++;

                //     }
                // });
                target[element.el] = null;
            }

            if (element.typeAg == 'sum') {
                if (target[element.el]) {
                    // console.log(target[element.el]);
                    // target[element.el] = `Sum: ${target[element.el].toFixed(2).toLocaleString()}`;
                    // console.log(target[element.el]);

                    let typeEl = typeof target[element.el];
                    // if(typeEl == 'string'){
                    //     console.log('this string');
                    //     target[element.el] += (Number(`${(target[element.el])}`));
                    // }else{
                    //     console.log('this Number');
                    target[element.el] = (`${(target[element.el]).toFixed(2)}`).toString();
                    // }
                    // target[element.el] = 'ผลรวม ： '+target[element.el] ;
                    target[element.el] = target[element.el];
                    // console.log(typeof target[element.el]);


                }
            }else if(element.typeAg == 'other'){
                // target[element.el] = null;
                
                params.forEachNodeAfterFilter((rowNode: RowNode) => {
                    if (rowNode.data[element.el]) {
                        // target[element.el] = `จำนวนรายการ : ${(params.getDisplayedRowCount())}`;
                        // target[element.el] = `จำนวนรายการ : ` + (i - 1);
                        target[element.el] = 'unvisible';
                    }else{
                        // target[element.el] = `จำนวนรายการ888 : ${(params.getDisplayedRowCount())}`;
                        target[element.el] = 'unvisible';
                    }
                });
                // console.log(target[element.el]);

                
            } else {
                params.forEachNodeAfterFilter((rowNode: RowNode) => {
                    if (rowNode.data[element.el]) {
                        target[element.el] = `จำนวนรายการ : ${(params.getDisplayedRowCount())}`;
                        // target[element.el] = `จำนวนรายการ : ` + (i - 1);
                    }
                });

            }

        })
        target[columnsWithAggregation[0].el] = 'แถวรวม ：'; 
        // console.log(target);    

        return target;
    }

    numberWithCommas(x) {
        var w = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return Number(w);
    }



}