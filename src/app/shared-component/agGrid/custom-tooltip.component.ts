import { Component } from '@angular/core';
import { ITooltipAngularComp } from 'ag-grid-angular';
import { ITooltipParams } from 'ag-grid-community';

@Component({
  selector: 'tooltip-component',
  template: ` <div class="custom-tooltip" [style.background-color]="color">
    <p *ngIf="field1Title"> <span>{{field1Title}} : {{ field1 }} </span></p>
    <p *ngIf="field2Title"> <span>{{field2Title}} : {{ field2 }} </span></p>
    <p *ngIf="field3Title"> <span>{{field3Title}} : {{ field3 }} </span></p>
    <p *ngIf="field4Title"> <span>{{field4Title}} : {{ field4 }} </span></p>
 
  </div>`,
  styles: [
    `
      :host {
        position: absolute;
        background-color: black;
        color: #fff;
        border-radius: 6px;
        padding:4px;
        width: auto;
        height: auto;
        pointer-events: none;
        transition: opacity 1s;
      }

      :host.ag-tooltip-hiding {
        opacity: 0;
      }

      .custom-tooltip p {
        margin: 5px;
        white-space: nowrap;
      }

      .custom-tooltip p:first-of-type {
        font-weight: bold;
      }
    `,
  ],
})
export class CustomTooltip implements ITooltipAngularComp {
  private params!: { color: string; field1Title: string; field2Title: string; field3Title: string; field4Title: string } & ITooltipParams;
  public data!: any;
  public color!: string;
  public field1Title!: string;
  public field2Title!: string;
  public field3Title!: string;
  public field4Title!: string;
  public field1!: string;
  public field2!: string;
  public field3!: string;
  public field4!: string;

  agInit(params: { color: string; field1Title: string; field2Title: string; field3Title: string; field4Title: string } & ITooltipParams): void {
    // console.log(params);
    this.params = params;

    this.data = params.api!.getDisplayedRowAtIndex(params.rowIndex!)!.data;
    this.color = this.params.color || 'white';
    this.field1Title = this.params.field1Title ;
    this.field2Title = this.params.field2Title ;
    this.field3Title = this.params.field3Title ;
    this.field4Title = this.params.field4Title ;
    // Get the field names specified in tooltipComponentParams and access the corresponding values from row data
    this.field1 = this.data[params.colDef.tooltipComponentParams.field1] || '-';
    this.field2 = this.data[params.colDef.tooltipComponentParams.field2] || '-';
    this.field3 = this.data[params.colDef.tooltipComponentParams.field3] || '-';
    this.field4 = this.data[params.colDef.tooltipComponentParams.field4] || '-';


  }
}
