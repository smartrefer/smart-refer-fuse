import { Injectable, } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class NumberValueTransform   {  
  

    currencyFormatter(params) {      
        var sansDec = Number(params.value).toFixed(2);
        var formatted = sansDec.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return `${formatted}`;
       
      }


}