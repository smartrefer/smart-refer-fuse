import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-custom-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.css']
})
export class CustomPaginationComponent implements OnChanges {
 
  @Input() childData: any;
  /* custom pagination */
  gridApi: any;
  rowsPerPage: any;
  dataLength: any;
  itemsdropdownPage: any[];
  selectedItem: any;
  filteredItems: any[];
  currentPage: any;
  totalPage: any;
  totalRows: any;
  pageSizeDatas:any[];
  selectedpageSizeDatas:any;
  pageNumText:any = 'เลือกไปหน้าที่.';

  constructor() {

  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log(this.childData);
    this.gridApi = this.childData.gridApi;
    this.rowsPerPage = this.childData.rowsPerPage;
    this.dataLength = this.childData.dataLength;
    // this.pageSizeDatas = this.childData.pageSizeDatas;
    // console.log(this.gridApi);
    this.generatePageNum(this.rowsPerPage, this.dataLength);
    this.currentPage = this.childData.currentPage;
    // this.totalPage =  this.childData.gridApi.paginationGetTotalPages();

  }



  /* custom pagination ------------------------------------------------------------------------*/

  generatePageNum(rowsPerPage, datalen) {
    this.totalRows = datalen;
    let page = Math.ceil(datalen / rowsPerPage);
    this.totalPage = page;

    this.itemsdropdownPage = [];
    for (let i = 1; i <= page; i++) {
      this.itemsdropdownPage.push({ label: 'หน้าที่ ' + i, value: i });
    }
  }


  filterItems(event) {
    console.log('filterItems');
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.itemsdropdownPage.length; i++) {
      let item = this.itemsdropdownPage[i];
      // if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(item);
      }
    }

    this.filteredItems = filtered;
  }

  onBtFirst() {
    this.gridApi.paginationGoToFirstPage();
    this.pageNumText = 'เลือกไปหน้าที่.';
  }

  onBtLast() {
    this.gridApi.paginationGoToLastPage();
    this.pageNumText = 'เลือกไปหน้าที่.';
  }

  onBtNext() {
    this.gridApi.paginationGoToNextPage();
    this.pageNumText = 'เลือกไปหน้าที่.';
  }

  onBtPrevious() {
    this.gridApi.paginationGoToPreviousPage();
    this.pageNumText = 'เลือกไปหน้าที่.';

  }
  onBtPageNum(e) {
    // console.log(e);
    // console.log(this.gridApi);
    let page_num: any = (e.value) - 1;
    this.pageNumText = 'หน้าที่คุณเลือก : ' +e.value;
    console.log(page_num);
    this.gridApi.paginationGoToPage(page_num);
  }

 


}
