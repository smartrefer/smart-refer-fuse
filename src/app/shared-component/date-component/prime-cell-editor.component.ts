import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AgEditorComponent } from 'ag-grid-angular';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-picker-cell-editor',
  template: `
 
    <p-calendar
    #container
    [(ngModel)]="value"
    [appendTo]="'body'"
    [style]="{ height: '100%', width: '100%' }"
    [inputStyle]="{ height: '100%', width: '100%' }"
    [locale]="en"
   
    [showIcon]="true"
    yearRange="2015:2060"
    dateFormat="d/m/yy"
   
    (onSelect)="onSelect($event)"
  
  ></p-calendar>
    
  `,
  styles: [``],
})
export class PrimeCellEditorComponent
  implements AgEditorComponent, AfterViewInit {
  params: any;
  value: Date;

  
//   <p-calendar
//   #container
//   [(ngModel)]="value"
//   [appendTo]="'body'"
//   [style]="{ height: '100%', width: '100%' }"
//   [inputStyle]="{ height: '100%', width: '100%' }"
//   [monthNavigator]="true"
//   [showIcon]="true"
//   yearRange="2015:2060"
//   dateFormat="d/m/yy"
//   (onSelect)="onSelect($event)"
// ></p-calendar>
  @ViewChild('container', { static: true }) public container;
  en:any = {
  
    dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหสบดี", "ศุกร์", "เสาร์"],
    dayNamesShort: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหสบดี", "ศุกร์", "เสาร์"],
    dayNamesMin: ["อา","จ","อ","พ","พฤ","ศ","ส"],
    monthNames: [ "ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค." ],
    monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'mm/dd/yy',
    weekHeader: 'Wk'
};
  constructor(private config: PrimeNGConfig) {
    this.setLangEN();
  }
  agInit(params: any): void {
    this.params = params;

    if (this.params.value) {
      const dateArray = this.params.value.split('/');

      const day = parseInt(dateArray[0]);
      const month = parseInt(dateArray[1]);
      const year = parseInt(dateArray[2]);

      this.value = new Date(year, month - 1, day);
    }

   
  }
  ngOnInit() {
    this.config.setTranslation({
      accept: 'Accept',
      reject: 'Cancel',
      //translations
  });
    
}

  // open the calendar when grid enters edit mode, i.e. the datepicker is rendered
  ngAfterViewInit() {
    this.container.toggle();
  }
  setLangEN() {
    // this.container.firstDayOfWeek = 1;
    this.config.setTranslation(this.en);
    // this.message = "Open the calendar now, monthes are ok, but watch days, they are not translated";
  }
  // ensures that once a date is selected, the grid will exit edit mode and take the new date
  // otherwise, to exit edit mode after a selecting a date, click on another cell or press enter
  onSelect(event) {
    console.log(event)
    this.params.api.stopEditing(false);
  }

  getValue() {
    const d = this.value;
    return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
  }
  onKeyDate(event: any){
    console.log(event);
    event.target.value = event.target.value.replace(/^(\d{2})(\d{2})(\d{4})/, '$1-$2-$3');
   }
}
