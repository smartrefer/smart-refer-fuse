import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-loading-overlay',
  template: `
  <input type="text" [textMask]="mask" [(ngModel)]="myData" style="width:100%;border:none">
  `,
  styles: [``],
})

export class CustomDateComponent {
  date: Date;
  params: any;
  myData: any;
  public mask = {
    guide: true,    
    showMask: true, 
    mask: [/\d/, /\d/, ':', /\d/, /\d/,]
  };
  
}
