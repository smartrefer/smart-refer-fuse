import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
@Injectable({
  providedIn: 'root'
})

export class DateValueTransform {

  constructor() { }

  getDateGetter(params) {
    let b: any = params.column.colId;
    // console.log(b);
    let c: any = 'params.data.'.concat(`${b}`);
    // console.log(c);
    let d: any = eval(c);
    console.log(d);
    // var t = moment(params.data.rate_date).tz('Asia/Bangkok').format('YYYY-MM-DD');
    if (d == 'unvisible') {
      return '';
    } else {
      if (d != null) {
        var t = moment(d).tz('Asia/Bangkok').format('YYYY-MM-DD');
        var parts = t.split('-');
        var mydate = parts[2].substr(0, 2) + '/' + parts[1] + '/' + (Number(parts[0]) + 543);
      } else {
        var mydate = '';
      }
      // console.log(mydate);
      return mydate;
    }
  }

  getDateSetter(params) {
    if (params.oldValue !== params.newValue) {
      console.log(params);
      let b: any = params.column.colId;
      console.log(b);
      let c: any = 'params.data.'.concat(`${b}`);
      var parts = params.newValue.split('/');
      console.log(parts);
      
      var mydate = (Number(parts[2]) - 543) + '-' + ('0' + parts[1]).substr(-2) + '-' + ('0' + parts[0]).substr(-2);
      console.log(mydate);
      
      eval(`${c} = mydate`);
      return true;
    } else {
      return false;
    }

  }


}