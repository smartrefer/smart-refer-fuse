import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AgEditorComponent } from 'ag-grid-angular';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-custom-datetime',
  template: ` 
  
       
    <p-calendar
    #container
    [inputStyle]="{'width':'80%'}"
    [(ngModel)]="value"
    [appendTo]="'body'"   
    
    [locale]="en"  
    [showIcon]="true"   
    showOn="button"
    showButtonBar="true"
    hideOnDateTimeSelect="false"
    inputId="time"
    showTime="true" 
    hourFormat="24"
    dateFormat="dd/mm/yy"
    [showOnFocus]="false"
    
    (onSelect)="onSelect($event)"
  ></p-calendar>
    
  `,
  styles: [``],
})
// <p-inputMask mask="99/99/9999" [(ngModel)]="value" placeholder="99/99/9999 00:00" slotChar="mm/dd/yyyy hh:mm"></p-inputMask>
export class CustomDatetimeComponent
  implements AgEditorComponent, AfterViewInit {
  params: any;
  value: Date;
 
  @ViewChild('container', { static: true }) public container;
  en:any = {
  
    dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหสบดี", "ศุกร์", "เสาร์"],
    dayNamesShort: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหสบดี", "ศุกร์", "เสาร์"],
    dayNamesMin: ["อา","จ","อ","พ","พฤ","ศ","ส"],
    monthNames: [ "ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค." ],
    monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'mm/dd/yy',
    weekHeader: 'Wk'
};
  constructor(private config: PrimeNGConfig) {
    this.setLangEN();
  }
  agInit(params: any): void {
    this.params = params;

    if (this.params.value) {
        const p = this.params.value.split(' ');
      const dateArray = p[0].split('/');
      const timeArray = p[1].split(':');

      const day = parseInt(dateArray[0]);
      const month = parseInt(dateArray[1]);
      const year = parseInt(dateArray[2]);
      const h =  parseInt(timeArray[0]);
      const m =  parseInt(timeArray[1]);

      this.value = new Date(year, month - 1, day,h, m, );
    }

   
  }
  ngOnInit() {
    this.config.setTranslation({
      accept: 'Accept',
      reject: 'Cancel',
      //translations
  });
    
}

  // open the calendar when grid enters edit mode, i.e. the datepicker is rendered
  ngAfterViewInit() {
    this.container.toggle();
  }
  setLangEN() {
    // this.container.firstDayOfWeek = 1;
    this.config.setTranslation(this.en);
    // this.message = "Open the calendar now, monthes are ok, but watch days, they are not translated";
  }
  // ensures that once a date is selected, the grid will exit edit mode and take the new date
  // otherwise, to exit edit mode after a selecting a date, click on another cell or press enter
  onSelect(event) {
    console.log(event)
    // this.params.api.stopEditing(false);
  }
 
  getValue() {
    const d = this.value;
    const t = ('0'+d.getHours()).substr(-2)+':'+('0'+d.getMinutes()).substr(-2);
    return `${('0'+d.getDate()).substr(-2)}/${d.getMonth() + 1}/${d.getFullYear()} ${t}`;
  }
}
