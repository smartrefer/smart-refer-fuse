import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-time-mask',
  template: `
  <input #yourControlName autofocus  type="text" [textMask]="mask"  [(ngModel)]="myTime"  (keypress)="onKeyPress($event)" (keydown)="onKeyDown($event)"> 
  `,
  styleUrls: ['./time-mas-component.css'], 
})

export class TimeMaskComponent implements AfterViewInit {
  @ViewChild('yourControlName') textInput;
  date: Date;
  params: any;
  myTime: any;
  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, ':', /\d/, /\d/]
  };
  
 

  
  // agInit(params: any): void {
  //   this.params = params;
  // }
  agInit(params: any): void {
    this.params = params;

    if (this.params.value) {     

      this.myTime = this.params.value.substring(0,5);
      console.log(this.myTime);
     
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.textInput.nativeElement.focus();
      this.textInput.nativeElement.setSelectionRange(0, 1);
    });
  }
  // onSelect(date) {
  //   this.date = date;
  //   this.params.onDateChanged();
  // }

  // onClearClick(event) {
  //   this.params.onDateChanged();
  // }

  // onInput(event) {
  //   this.params.onDateChanged();
  // }
  // onClickk(){
  //   // console.log(this.params);
  // }
  onKeyPress(event) {
 
    if (!isNumeric(event) ) {
      event.preventDefault();
    } 
 

    function isNumeric(ev) {
      return /\d/.test(ev.key);
    }
  }

  onKeyDown(event) {
    // left and right arrow
    if (event.keyCode === 39 || event.keyCode === 37) {
      console.log(event.value);
      event.stopPropagation();
    }
  }


  // getDate(): Date {
  //   return this.date;
  // }

  // setDate(date: Date): void {
  //   this.date = date || null;
  // }

  getValue() {
    const d = this.myTime;
    return d+":00";
  }
}
