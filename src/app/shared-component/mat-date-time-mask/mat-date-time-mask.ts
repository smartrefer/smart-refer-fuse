import { Component, OnInit, ElementRef, Renderer2, ViewChild, AfterViewInit, ViewContainerRef } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AgEditorComponent } from 'ag-grid-angular';
import { PrimeNGConfig } from 'primeng/api';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/menu/format-datepicker';
import * as moment from 'moment-timezone';

/** @title Basic datepicker */
@Component({
    selector: 'mat-date-time-mask',
    templateUrl: 'mat-date-time-mask.html',
    styleUrls: ['mat-date-time-mask.css'],
    providers: [
        { provide: DateAdapter, useClass: AppDateAdapter },
        { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
    ]
})
export class MatDateTimeMask implements AgEditorComponent, AfterViewInit {
    params: any;
    value: Date;
    datePicker: Date;
    datePicker1: Date;
    datePickerTemp: any;
    events: string[] = [];
    @ViewChild('picker', { static: true }) public container;
    @ViewChild('picker2', { static: true }) public picker2;

    @ViewChild('container') aForm: ElementRef;

    @ViewChild('datemat', { static: true }) datemat: any;

    @ViewChild('datemat', { read: ViewContainerRef }) public datematt: any;
    // @ViewChild('datePickerTemp',  { static: false }) datePickerTemp3: any;

    // @ViewChild('datePickerTemp3', { static: true }) datePickerTemp3: any;
    @ViewChild('datePickerTemp3', { read: ViewContainerRef }) public datePickerTemp3: ViewContainerRef;

    @ViewChild('datePickerTempp', { read: ViewContainerRef }) public datePickerTempp: any;
    @ViewChild('datepickerFooter', { static: false }) public datepickerFooter;
    public highlightAllOnFocus: boolean = true;
    public mask = {
        guide: true,
        showMask: true,
        mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
    };
    constructor(
        private config: PrimeNGConfig,
        private renderer: Renderer2
    ) {

    }
    // ngOnInit(): void {
    //     console.log(this.params);

    // }

    // agInit(params: any): void {
    //     console.log(params);
    //     this.params = params;
    //     if (this.params.value) {
    //         const dateArray = this.params.value.split('/');
    //         const day = parseInt(dateArray[0]);
    //         const month = parseInt(dateArray[1]);
    //         const year = parseInt(dateArray[2]) - 543;
    //         console.log("d:" + day + " m:" + month + " y:" + year);
    //         console.log(new Date(year, month - 1, day));
    //         this.value = new Date(year, month - 1, day);
    //         this.datePicker = new Date(year, month - 1, day);
    //         this.datePickerTemp = params.value;
    //         // console.log(this.value);
    //     }

    // }

    agInit(params: any): void {
        console.log(params);
        this.params = params;

        if (this.params.value) {
            const p = this.params.value.split(' ');
            const dateArray = p[0].split('/');
            const timeArray = p[1].split(':');

            const day = parseInt(dateArray[0]);
            const month = parseInt(dateArray[1]);
            const year = parseInt(dateArray[2]);
            const h = parseInt(timeArray[0]);
            const m = parseInt(timeArray[1]);

            this.value = new Date(year, month - 1, day, h, m);
            this.datePicker = new Date(year-543, month - 1, day, h, m);
            console.log(this.datePicker);
            this.datePickerTemp = params.value;
        }

    }

    ngAfterViewInit() {
        // console.log(this.datePickerTemp3);        
        setTimeout(() => this.datePickerTemp3.element.nativeElement.children[0].focus());

    }

    onOpen() {
        this.appendFooter();
    }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        this.events.push(`${type}: ${event.value}`);
    }

    // onSelect(type: string, event: MatDatepickerInputEvent<Date>) {
    //     console.log(event)
    //     const val = event.value;
    //     if (val.getDate) {
    //         const y = parseInt(`${val.getFullYear()}`);
    //         const m = parseInt(`${val.getMonth()}`);
    //         const d = parseInt(`${val.getDate()}`);
    //         this.value = new Date(y, m, d);
    //         console.log(y);
    //         console.log(this.value);
    //         this.params.api.stopEditing(false);
    //     }
    // }
    onSelect(type: string, event: MatDatepickerInputEvent<Date>) {
        let dateString = event.value.toLocaleString();
        let newDate = new Date(dateString);
        console.log(event);
        console.log(event.value.toLocaleString());
        console.log(newDate.getDate());  

        if (newDate.getDate()) {
            const y = parseInt(`${newDate.getFullYear()}`);
            const m = parseInt(`${newDate.getMonth()}`);
            const d = parseInt(`${newDate.getDate()}`);
            const h = parseInt(`${newDate.getHours()}`);
            const mm = parseInt(`${newDate.getMinutes()}`);
            this.value = new Date(y+543, m, d,h,mm);
            this.datePicker = new Date(y+543, m, d,h,mm);
            // console.log(y);
            console.log(this.value);
            this.params.api.stopEditing(false);
        } else {
            console.log('nnnn');
            this.params.api.stopEditing(false);
        }
    }
    onInput(type: string, event: MatDatepickerInputEvent<Date>) {
        console.log(event);
    }

    getValue() {
        console.log('getvalue');
        const d = this.value;
        console.log(d);
        const t = ('0'+d.getHours()).substr(-2)+':'+('0'+d.getMinutes()).substr(-2);
        let hh = parseInt(('0'+d.getHours()).substr(-2));
        const mi = parseInt(('0'+d.getMinutes()).substr(-2));       
     
       
        this.datePicker = new Date(d);

        return `${('0'+d.getDate()).substr(-2)}/${d.getMonth() + 1}/${d.getFullYear()-543} ${t}`;
        // return `${(d.getFullYear())-543}-${d.getMonth() + 1}-${('0'+d.getDate()).substr(-2)} ${t}`;

        // const y = parseInt(`${d.getFullYear()}`);
        // const m = parseInt(`${d.getMonth()}`);
        // const dd = parseInt(`${d.getDate()}`);
       
       
        // const datet =  `${y}-${m + 1}-${dd} ${t}`;
        // console.log(datet);

        // this.value = new Date(y, m - 1, dd, hh, mi,);
        // this.datePicker = new Date(y, m - 1, dd, hh, mi,);
        // return datet;

        // const p = el.split(' ');
        // const dateArray = p[0].split('/');
        // const timeArray = p[1].split(':');

        // const day = parseInt(dateArray[0]);
        // const month = parseInt(dateArray[1]);
        // const year = (parseInt(dateArray[2]))-543;
        // const h = parseInt(timeArray[0]);
        // const m = parseInt(timeArray[1]);

      
      }


    // getValue() {
    //     // const val = this.value;
    //     const val = this.datePicker;

    //     // console.log(val);
    //     const y = parseInt(`${val.getFullYear()}`) + 543;
    //     const m = parseInt(`${val.getMonth()}`);
    //     const d = parseInt(`${val.getDate()}`);
    //     return `${d}/${m + 1}/${y}`;
    // }
    setFocus(name) {
        const element = this.renderer.selectRootElement('#container');
        // const element = this.aForm.nativeElement[name];  
        console.log(element);
        if (element) {
            // element.focus();
            setTimeout(() => element.focus(), 0);
        }
    }
    focusInput(input) {
        input.focus();
    }
    // today() {
    //     this.value = new Date();
    //     this.container.close();
    //     // console.log('today', this.datepicker);
    //     // this.datepicker.nativeElement.close();
    //     this.params.api.stopEditing(false);
    // }
    today() {
        this.value = new Date();
        this.datePicker = new Date();
        this.picker2.close();
        this.params.api.stopEditing(false);
    }
    done(t:string,e:MatDatepickerInputEvent<Date>) {
        console.log('done');
        // this.container.close();
        this.onSelect(t,e);
    }
    showCalendar() {
        //   console.log(this);
        this.container.open();
        this.datematt.element.nativeElement.select();
        this.datematt.element.nativeElement.setSelectionRange(0, 1);

    }
    private appendFooter() {
        const matCalendar = document.getElementsByClassName('mat-datepicker-content')[0] as HTMLElement;
        matCalendar.appendChild(this.datepickerFooter.nativeElement);
    }

    onDobSelected() {
        // console.log(this.emp_dob);
        var t = moment(this.datePicker).tz('Asia/Bangkok').format('YYYY-MM-DD');
        var parts = t.split('-');
        var mydate = parts[2].substr(0, 2) + '/' + parts[1] + '/' + (Number(parts[0]) + 543);
        // console.log(mydate);
        this.datePickerTemp = mydate;

    }
    dateBlur(el) {
        console.log('onblur');
        console.log(el);
        if (el != '') {
            // var parts = el.split('/');
            // var mydate = (Number(parts[2]) - 543) + '-' + parts[1] + '-' + parts[0];
            // var d = parts[0];
            // var m = parts[1] - 1;
            // var y = Number(parts[2]) - 543;
            // // this.datePicker = mydate;
            // this.datePicker = new Date(y, m, d);;
            // console.log(this.datePicker);
            
            const p = el.split(' ');
            const dateArray = p[0].split('/');
            const timeArray = p[1].split(':');

            const day = parseInt(dateArray[0]);
            const month = parseInt(dateArray[1]);
            const year = (parseInt(dateArray[2]));
            const h = parseInt(timeArray[0]);
            const m = parseInt(timeArray[1]);

            this.value = new Date(year, month - 1, day, h, m,);
            this.datePicker = new Date(year-543, month - 1, day, h, m,);
            console.log(this.datePicker);
            // this.params.api.stopEditing(false);
         
        }

    }
    keyEnterSave(event) {
        let el = event.target.value;
        // console.log(event);
        // console.log('keydown');
        if (event.keyCode === 13 || event.keyCode === 9) {
            // console.log(event);           
            // const p = el.split(' ');
            // const dateArray = p[0].split('/');
            // const timeArray = p[1].split(':');
            // const day = parseInt(dateArray[0]);
            // const month = parseInt(dateArray[1]);
            // const year = (parseInt(dateArray[2]));
            // const h = parseInt(timeArray[0]);
            // const m = parseInt(timeArray[1]);
            // this.value = new Date(year, month - 1, day, h, m,);
            // this.datePicker = new Date(year, month - 1, day, h, m,);
            // console.log(this.datePicker);   
            this.dateBlur(el);    

            this.params.api.stopEditing(false);
            
        }
    }


}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */

    