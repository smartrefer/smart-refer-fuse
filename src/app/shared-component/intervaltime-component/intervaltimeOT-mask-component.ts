import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MenuItem, Message, MessageService, ConfirmationService, PrimeNGConfig, ConfirmEventType } from 'primeng/api';
import { stringify } from 'querystring';


@Component({
    selector: 'app-intervaltimeot-mask',
    template: `
 
  <p-inputMask inputId="yourControlName"  type="text" mask="99:99" slotChar="ชม:นท" [(ngModel)]="myTime" 
   (keypress)="onKeyPress($event)"  (keyup)="onKeyDown($event)"    >
   </p-inputMask>
  
  `,
    styleUrls: ['./intervaltime-mask-component.css'],
})
// (keyup)="onKeyDown($event)"
// <input #yourControlName autofocus  type="text" [textMask]="mask"  [(ngModel)]="myTime"  (keypress)="onKeyPress($event)" (keydown)="onKeyDown($event)"  > 
export class IntervalTimeOtMaskComponent implements AfterViewInit {
    @ViewChild('yourControlName') textInput;
    oldVal: any;
    date: Date;
    params: any;
    myTime: any;
    myTimeSend: any;
    myHour: any;
    myMinute: any;
    mySecond: any;
    myMon: any;
    myDay: any;
    validate_date: boolean;
    cancelCheck: boolean;


    constructor(

        private messageService: MessageService) { }

    // agInit(params: any): void {
    //   this.params = params;
    // }
    agInit(params: any): void {
        console.log(params);
        this.oldVal = params.value;
        this.params = params;
        let b = this.params.value.replace('days', 'day').replace('mons', 'mon').replace(' ', '');
        console.log(b);
        // let text = "1 mon 2 day 01:00:00";
        // const myArray = text.split("mon");

        if (this.params.value) {
            let t = b.split("day");
            console.log(t);
            console.log(t.length);
            let checkDay: boolean = b.includes("day");
            let checkTime: boolean = b.includes(":");
            console.log(checkDay);
            console.log(checkTime);
            // this.myTime = t[1];
            if (checkDay) {
                // let dd = (t[0].toString()).split('day');
                this.myDay = ('0' + parseInt(t[0])).substring(3, 0);

            } else {
                this.myDay = '00';
            }


            if (checkTime && t.length > 1) {
                console.log('1');
                let tt = (t[1].toString()).split(':');
                this.myHour = tt[0];
                this.myMinute = tt[1];
                this.mySecond = tt[2];
            } else if (checkTime && t.length == 1) {
                console.log('2');

                let tt = (t[0].toString()).split(':');
                this.myHour = tt[0];
                this.myMinute = tt[1];
                this.mySecond = tt[2];
            } else {
                console.log('3');

                this.myHour = '00';
                this.myMinute = '00';
                this.mySecond = '00';
            }


            this.myTime = this.myHour + ':' + this.myMinute;

        }
    }
    ngAfterViewInit() {
        setTimeout(() => {
            document.getElementById("yourControlName").focus();
            //   let dom =   document.getElementById("yourControlName");
            //  dom[0].setSelectionRange(1, 1);

            // this.textInput.nativeElement.focus();
            // this.textInput.nativeElement.setSelectionRange(0, 1);
        });
    }
    onFocus() {

        // console.log('me');

    }

    onKeyPress(event) {
        console.log(event);
        console.log(event.keyCode);
        console.log(this.myTime);
        this.myTimeSend = this.myTime;

    }

    onKeyDown(event) {
        // 
        console.log(event);
        if (event.keyCode == 27) {
            console.log("esc");
            this.myTimeSend = this.oldVal;
            this.params.stopEditing();
            return this.oldVal;

        }
        // console.log(this.myTime);
    }


    // getDate(): Date {
    //   return this.date;
    // }

    // setDate(date: Date): void {
    //   this.date = date || null;
    // }

    // Gets called once before editing starts, to give editor a chance to
    // cancel the editing before it even starts.
    isCancelBeforeStart() {
        return false;
    }

    // Gets called once when editing is finished (eg if Enter is pressed).
    // If you return true, then the result of the edit will be ignored.
    isCancelAfterEnd() {
        let tt: string;
        let days: string;
        let h: string;
        let m: string;
        let isNumber: boolean;
        if (/\d/.test(this.myTimeSend)) {
            isNumber = true;
            // console.log(this.myTimeSend + " has a number.")
            let arrMyTimeSend = this.myTimeSend.split("/");
            console.log(arrMyTimeSend);


            if (/\d/.test(arrMyTimeSend[0])) {
                let t = arrMyTimeSend[0].split(":");
                if (/\d/.test(t[0])) {
                    h = this.rightCharactor(('0' + parseInt(t[0])), 2);
                } else {
                    h = '00';
                }
                if (/\d/.test(t[1])) {
                    m = this.rightCharactor(('0' + parseInt(t[1])), 2);
                } else {
                    m = '00';
                }
            } else {
                h = '00';
                m = '00';
            }


            tt = h + ":" + m;
            console.log(tt);
            this.myTime = tt;
            // return this.myTime == this.myTime;

        } else {
            isNumber = false;
            let detail: string = '';
            if (this.cancelCheck) {
                detail = 'ยกเลิกการกรอกข้อมูล';
            } else {
                detail = 'ยังไม่ได้กรอกข้อมูล';
            }
            this.messageService.add({ severity: 'error', summary: 'ผลของกระบวนการ', detail: detail });
            return this.myTime == '';
        }


    }

    rightCharactor(str, chr) {
        let newstr: string;
        return newstr = str.substr(str.length - chr, str.length)
    }
    leftCharactor(str, chr) {
        let newstr: string;
        return newstr = str.substr(0, chr)
    }
    getValue() {
        // let d:string = this.myTime;
        // d.replace("/","day")+':00';
        // console.log(d);
        // return d;
        let arrTime = this.myTime.split("/");
        // console.log(arrTime);
        if (arrTime == 'NaN dayundefined:00' || arrTime[0] === "") {
            console.log('not valid format');
            // this.params.stopEditing();

        } else {
            console.log(arrTime);
            // let arrTime = this.myTime.split("/");

            let t: string = arrTime[0] + ':00';
            let sendTime: string = t;
            // console.log(sendTime);
            return sendTime;
        }

    }
}
