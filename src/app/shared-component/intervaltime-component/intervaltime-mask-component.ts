import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MenuItem, Message, MessageService, ConfirmationService, PrimeNGConfig, ConfirmEventType } from 'primeng/api';


@Component({
  selector: 'app-intervaltime-mask',
  template: `
 
  <p-inputMask inputId="yourControlName"  type="text" mask="99 / 99:99" slotChar="วว / ชม:นท" [(ngModel)]="myTime" 
   (keypress)="onKeyPress($event)"   (keyup)="onKeyDown($event)"  >
   </p-inputMask>
  
  `,
  styleUrls: ['./intervaltime-mask-component.css'],
})
// (keyup)="onKeyDown($event)"
// <input #yourControlName autofocus  type="text" [textMask]="mask"  [(ngModel)]="myTime"  (keypress)="onKeyPress($event)" (keydown)="onKeyDown($event)"  > 
export class IntervalTimeMaskComponent implements AfterViewInit {
  @ViewChild('yourControlName') textInput;
  oldVal:any;
  date: Date;
  params: any;
  myTime: any;
  myTimeSend: any;
  myHour: any;
  myMinute: any;
  mySecond: any;
  myMon: any;
  myDay: any;
  validate_date: boolean;
  cancelCheck:boolean;

  constructor(

    private messageService: MessageService) { }

  // agInit(params: any): void {
  //   this.params = params;
  // }
  agInit(params: any): void {
    console.log(params);
    this.params = params;
    let b = this.params.value.replace('days', 'day').replace('mons', 'mon').replace(' ', '');
    console.log(b);
    // let text = "1 mon 2 day 01:00:00";
    // const myArray = text.split("mon");

    if (this.params.value) {
      let t = b.split("day");
      console.log(t);
      console.log(t.length);
      let checkDay:boolean = b.includes("day");
      let checkTime:boolean = b.includes(":");
      console.log(checkDay);
      console.log(checkTime);
      // this.myTime = t[1];
      if(checkDay){
        // let dd = (t[0].toString()).split('day');
        this.myDay = ('0' + parseInt(t[0])).substring(3, 0);

      }else{
        this.myDay = '00';
      }

      
      if(checkTime && t.length>1){  
        console.log('1');    
          let tt = (t[1].toString()).split(':');  
          this.myHour = tt[0];
          this.myMinute = tt[1];
          this.mySecond = tt[2];         
      }else if(checkTime && t.length==1){
        console.log('2');    

        let tt = (t[0].toString()).split(':');  
          this.myHour = tt[0];
          this.myMinute = tt[1];
          this.mySecond = tt[2]; 
      }else{
        console.log('3');    

        this.myHour = '00';
        this.myMinute = '00';
        this.mySecond = '00';
      }

      // console.log(this.myHour);
      // console.log(this.myMinute);

      // console.log(this.mySecond);
      // console.log(this.myDay);
    

      this.myTime = this.myDay + ' / ' + this.myHour + ':' + this.myMinute;
    

    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      document.getElementById("yourControlName").focus();
      //   let dom =   document.getElementById("yourControlName");
      //  dom[0].setSelectionRange(1, 1);

      // this.textInput.nativeElement.focus();
      // this.textInput.nativeElement.setSelectionRange(0, 1);
    });
  }
  onFocus() {

    // console.log('me');

  }
  // onSelect(date) {
  //   this.date = date;
  //   this.params.onDateChanged();
  // }

  // onClearClick(event) {
  //   this.params.onDateChanged();
  // }

  // onInput(event) {
  //   this.params.onDateChanged();
  // }
  // onClickk(){
  //   // console.log(this.params);
  // }
  onKeyPress(event) {
    console.log(event);
    console.log(event.keyCode);
    console.log(this.myTime);
    this.myTimeSend = this.myTime;
    if (event.keyCode === 13) {
      this.myTimeSend = this.myTime;
      console.log("enter");
    }
  }

  onKeyDown(event) {
    // 
    console.log(event);
    if (event.keyCode == 27) {
        console.log("esc");
        this.myTimeSend = this.oldVal;
        this.cancelCheck =true;
        this.params.stopEditing();
        return this.oldVal;

    }
    // console.log(this.myTime);
}


  // getDate(): Date {
  //   return this.date;
  // }

  // setDate(date: Date): void {
  //   this.date = date || null;
  // }

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart() {
    return false;
  }

  // Gets called once when editing is finished (eg if Enter is pressed).
  // If you return true, then the result of the edit will be ignored.
  isCancelAfterEnd() {
    let tt: string;
    let days: string;
    let h: string;
    let m: string;
    let isNumber: boolean;
    if (/\d/.test(this.myTimeSend)) {
      isNumber = true;
      // console.log(this.myTimeSend + " has a number.")
      let arrMyTimeSend = this.myTimeSend.split("/");
      console.log(arrMyTimeSend);
      if (/\d/.test(arrMyTimeSend[0])) {
        days = ('0' + parseInt(arrMyTimeSend[0])).substring(3, 0);;
      } else {
        days = '00';
      }

      if (/\d/.test(arrMyTimeSend[1])) {
        let t = arrMyTimeSend[1].split(":");
        if (/\d/.test(t[0])) {
          h = ('0' + parseInt(t[0])).substring(3, 0);
        } else {
          h = '00';
        }
        if (/\d/.test(t[1])) {
          m = ('0' + parseInt(t[1])).substring(3, 0);
        } else {
          m = '00';
        }
      } else {
        h = '00';
        m = '00';
      }


      tt = days + " / " + h + ":" + m;
      console.log(tt);
      this.myTime = tt;
      // return this.myTime == this.myTime;

    } else {
      isNumber = false;
      let detail:string ='';
      if(this.cancelCheck){
        detail = 'ยกเลิกการกรอกข้อมูล';
      }else{
        detail = 'ยังไม่ได้กรอกข้อมูล';
      }
      this.messageService.add({ severity: 'error', summary: 'ผลของกระบวนการ', detail: detail });
      return this.myTime == '';
    }

    // our editor will reject any myTime is null 
    // return this.value > 1000;
    // console.log(this.myTimeSend);




    // if (this.myTimeSend == '') {
    //   this.messageService.add({ severity: 'error', summary: 'ผลของกระบวนการ', detail: `กรอกข้อมูลไม่ถูกต้อง` });
    // } else if (this.myTimeSend == undefined) {
    //   this.messageService.add({ severity: 'error', summary: 'ผลของกระบวนการ', detail: `ยังไม่ได้กรอกข้อมูล` });
    //   return this.myTime == '';
    // } else {
    //   this.myTimeSend;
    // }

  }
  getValue() {
    // let d:string = this.myTime;
    // d.replace("/","day")+':00';
    // console.log(d);
    // return d;
    let arrTime = this.myTime.split("/");
    // console.log(arrTime);
    if (arrTime == 'NaN dayundefined:00' || arrTime[0] === "") {
      console.log('not valid format');
      // this.params.stopEditing();

    } else {
      console.log(arrTime);
      // let arrTime = this.myTime.split("/");
      let d: number = parseInt(arrTime[0]);
      let t: string = arrTime[1] + ':00';
      let sendTime: string = d + ' day' + t;
      // console.log(sendTime);
      return sendTime;
    }

  }
}
